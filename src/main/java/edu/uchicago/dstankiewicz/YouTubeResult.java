/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.dstankiewicz;

/**
 *
 * @author asdf
 */
class YouTubeResult {
    
    private String url;
    private String title;
    
    public YouTubeResult(){
        
    }
    
    public YouTubeResult(String title, String url){
        this.title = title;
        this.url = url;
    }
    
    public void setTitle(){
        this.title = title;
    }
    
    public void setUrl(){
        this.url = url;
    }
    
    public String getTitle(){
        return this.title;
    }
    
    public String getUrl(){
        return this.url;
    }
    
}
