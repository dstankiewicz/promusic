package edu.uchicago.dstankiewicz;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.google.gdata.client.*;
import com.google.gdata.client.youtube.*;
import com.google.gdata.data.*;
import com.google.gdata.data.extensions.*;
import com.google.gdata.data.geo.impl.*;
import com.google.gdata.data.media.*;
import com.google.gdata.data.media.mediarss.*;
import com.google.gdata.data.youtube.*;
import com.google.gdata.util.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 *
 * @author asdf
 */
public class YouTubeSearch {
    
    public YouTubeSearch(){
        
    }
    
    public ArrayList<YouTubeResult> query(String qstring, int maxresults) throws IOException{
        ArrayList<YouTubeResult> ytr = new ArrayList();
        try{
            YouTubeService service = new YouTubeService("","");
            YouTubeQuery query = new YouTubeQuery(new URL("http://gdata.youtube.com/feeds/api/videos"));
            query.setFullTextQuery(qstring);
            query.setMaxResults(maxresults);
            query.setSafeSearch(YouTubeQuery.SafeSearch.NONE);
            VideoFeed videoFeed = service.query(query, VideoFeed.class);
            System.out.println(videoFeed.toString());
            ytr =  parseVideoFeed(videoFeed);
        }
        catch(ServiceException e){
            System.out.println("ERROR in search"+e);
        } catch (IOException e) {
            System.out.println("ERROR in search"+e);
        }
        return ytr;
    }

    private ArrayList<YouTubeResult> parseVideoFeed(VideoFeed videoFeed) {
        ArrayList<YouTubeResult> ytr = new ArrayList();
        for (VideoEntry ve : videoFeed.getEntries()){
//            System.out.println(ve.getTitle().getPlainText() + " - " + ve.getHtmlLink().getHref());
            ytr.add(new YouTubeResult(ve.getTitle().getPlainText(),ve.getHtmlLink().getHref()));
        }
        return ytr;
    }    
    
}
