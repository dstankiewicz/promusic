/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.dstankiewicz;

import de.umass.lastfm.Artist;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author asdf
 */
public class ProMusicController implements Initializable {
    @FXML
    private Button btnEdit;
    @FXML
    private Button btnSearch;
    @FXML
    private Button btnDownload;
    @FXML
    private Button btnRemove;
    @FXML
    private Button btnClear;
    @FXML
    private Button btnBrowse;
    @FXML
    private Button btnOutputDir;
    @FXML
    private Label txtOutputDir;
    @FXML
    private Label label;
    @FXML
    private ListView<String> lstResult;
    @FXML
    private TextField txtSearch;
    @FXML
    private TextField txtInputFile;
    @FXML
    private TableView<DownloadTask> tblDownloads;
    @FXML
    private TableView<LastFmSearch> tblResults;
    private ExecutorService downloadExecutor;
    private String DEFAULT_DIR_TEXT = "(required)";
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    
    private final String DEFAULT_LBLFILE = "- No file specified -";
    private final String DEFAULT_SEARCH_PLACEHOLDER = "Search terms ( ; separated )";
    File ifile;
    String outputdirectory;
    private HashMap<String,LastFmSearch> searchResults;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Long x = (long) 1;
        Long y = (long) 100;
        
        System.out.println("ProMusicController.initialize");
        
        TableColumn<LastFmSearch, String> searchResultColumn = new TableColumn("Best Match");
        searchResultColumn.setCellValueFactory(new PropertyValueFactory<LastFmSearch, String>("chosenArtist"));
        searchResultColumn.setPrefWidth(230);
        
        TableColumn<LastFmSearch, String> searchTermColumn = new TableColumn("Search Term");
        searchTermColumn.setCellValueFactory(new PropertyValueFactory<LastFmSearch, String>("searchTerm"));
        searchTermColumn.setPrefWidth(230);
        
        TableColumn<LastFmSearch, String> messageColumn = new TableColumn("Info");
        messageColumn.setCellValueFactory(new PropertyValueFactory<LastFmSearch, String>("message"));
        messageColumn.setPrefWidth(230);
        
        tblResults.getColumns().addAll(searchTermColumn,searchResultColumn,messageColumn);
        tblResults.editableProperty().set(true);
//        tblResults.getSelectionModel().setCellSelectionEnabled(true);
        tblResults.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        
        TableColumn<DownloadTask, String> artistColumn = new TableColumn("Artist");
        artistColumn.setCellValueFactory(new PropertyValueFactory<DownloadTask, String>("title"));
        artistColumn.setPrefWidth(200);
        TableColumn<DownloadTask, String> statusColumn = new TableColumn("Status");
        statusColumn.setCellValueFactory(new PropertyValueFactory<DownloadTask, String>("message"));
        statusColumn.setPrefWidth(200);
        TableColumn<DownloadTask, Double> progressColumn = new TableColumn("Progress");
        progressColumn.setCellValueFactory(new PropertyValueFactory<DownloadTask, Double>("progress"));
        progressColumn.setPrefWidth(300);
        progressColumn.setCellFactory(ProgressBarTableCell.<DownloadTask>forTableColumn());
        
        tblDownloads.getColumns().addAll(artistColumn,statusColumn,progressColumn);
        searchResults = new HashMap();
        outputdirectory = "";
     
    }

    private ArrayList<String> parseSearchTerms(String s){
        System.out.println("parseSeearchTerms");
        ArrayList<String> searchterms = new ArrayList();
        Scanner sc = new Scanner(s).useDelimiter(";");
        while(sc.hasNext()){
            String t = sc.next().trim();
            if (!t.isEmpty()){
                searchterms.add(t);
                System.out.println(t);
            }
        }
        return searchterms;        
    }
    
    @FXML
//    Search lastfm for particular artist and populate box
    private void handleBtnSearchAction(ActionEvent event) {
        System.out.println("btnSearchAction");
//                    if (artist.trim().length() >0){
////                lastfmtracklist = lfms.queryTracks(q, 1);
////                lastfmtracklist = lfms.queryArtist(q, 1);
//                lfms.queryArtist(artist);
//            }
//            else{
//                System.out.println("please enter search terms, dummy");
//            }
        
        String s = txtSearch.getText().trim();
        if (txtSearch.getText().trim().isEmpty()){
            System.out.println("btnSearchAction: search button pressed but no search terms entered");
        } else {
            ArrayList<String> searchterms = parseSearchTerms(txtSearch.getText().trim());
            System.out.println("btnSearchAction: terms parsed into this many chunks - " + searchterms.size());
            if (0<searchterms.size()){
                populateSearchResults(searchterms);
            }
            else{
                System.out.println("btnSearchAction: no valid terms were entered");
            }
        }
    }
  
    
    //Searches lastfm for each of the artists in the list given.  Wipes out existing search results!
    private void populateSearchResults(ArrayList<String> searchterms){
        System.out.println("populateSearchResults");
        searchResults.clear();
        tblResults.getItems().clear();
//        tblResults;
        for (String s : searchterms){
            LastFmSearch t = searchForArtist(s);
            if (0 == t.getAllArtists().size()){
                System.out.println("populateSearchResults: " + s + " ===>  NO RESULT FOUND!!!!!");
            }
            else{
                searchResults.put(s,t);
                System.out.println("populateSearchResults: top result for " + s + " ===> " + t.getTopArtist());
            }
//            System.out.println("populateSearchResults: search for " + s + " returned this many results - " + t.getAllArtists().size());
        }
        
        try {
            for (String s : searchResults.keySet()){
                tblResults.getItems().add(searchResults.get(s));
                
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        
        
    }
    
    private LastFmSearch searchForArtist(String artist){
        System.out.println("searchForArtist");
        LastFmSearch lfms = new LastFmSearch();
        boolean result=false;
        try {
            System.out.println("Searching for artist ... "+artist);
            if (artist.trim().length() >0){
                result = lfms.queryArtist(artist);
            }
            else{
                System.out.println("searchForArtist: search term was empty ...");
            }
        } catch (IOException ex) {
            Logger.getLogger(ProMusicController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lfms;
    }
    
    
    @FXML
//    Search youtube via a YouTubeSearch object
    private void handleBtnClearAction(ActionEvent event) {
        System.out.println("handleClearAction");
        tblResults.getItems().clear();
//        btnClear.disable();
    }
    
    @FXML
    private void handleBtnDowload(ActionEvent event) {
        System.out.println("handleBtnDownload");
        ObservableList<LastFmSearch> a = tblResults.getItems();
        
        if (outputdirectory.isEmpty()){
            System.out.println("btnDownload: NEED OUTPUT DIR");
            txtOutputDir.setStyle("-fx-border-color: red;");
            return;
        }
        
        if (a.isEmpty()){
            System.out.println("btnDownload: NOTHING TO DOWNLOAD");
            return;
        }
        else{
            if (!tblResults.getSelectionModel().getSelectedItems().isEmpty()){//download all items in the list
                a = tblResults.getSelectionModel().getSelectedItems();
                System.out.println("btnDownload: Only DL selected items");
            }
        }
        
        //We have at least one item in the 
        downloadExecutor = Executors.newFixedThreadPool(3, new ProMusicThreadFactory());
        for(LastFmSearch lfms : a){

            File f = new File(txtOutputDir.getText());
//            if (f.exists() && f.isDirectory()){
//                
//            } else {
//                System.out.println("btnDownload: Invalid path " + odir.getPath());
//                return;
//            }
            
            String s = lfms.getChosenArtist();
            if (0<s.length()){                
                DownloadTask d1 = new DownloadTask(s,new File(outputdirectory));
                tblDownloads.getItems().add(d1);
                downloadExecutor.execute(d1);
            }
        }

        System.out.println("btnDownload exiting ...");
    }
    
    @FXML
    private void handleBtnRemove(ActionEvent event) {
        System.out.println("handleBtnRemove");
        ObservableList<Integer> selectedstuf = tblResults.getSelectionModel().getSelectedIndices();
        for (int index : selectedstuf){
            tblResults.getItems().remove(index);
        }
    }
   
    @FXML
    private void handleBtnLoadFromFile(ActionEvent event) {
        System.out.println("handleBtnLoadFromFile");
        try{
//            Scanner sc = new Scanner(ifile.getPath()).;
            Scanner sc = new Scanner(ifile).useDelimiter(";");
            ArrayList<String> searchterms = new ArrayList();
            String a = "";
            while(sc.hasNext()){
                a = sc.next().trim();
                if (0<a.length()){
                    searchterms.add(a);
                }
            }
            populateSearchResults(searchterms);
        }catch(FileNotFoundException e){
            System.out.println("Error opening file");
        }
        
    }
    
        @FXML
    private void handleBtnBrowseOutputDir(ActionEvent event) {
        System.out.println("handleBtnOutputDir");
        DirectoryChooser dc = new DirectoryChooser();

        try{
            File odir = dc.showDialog(null);
            txtOutputDir.textProperty().setValue(odir.getName() + "  ("+odir.getPath()+")");
            outputdirectory = odir.getPath();
            txtOutputDir.setStyle("-fx-border-color: green;");
        }catch(Exception e){
            System.out.println("Error opening dir ... " + e);
        }
        
    }
    
  
    
    @FXML
    private void handleBtnBrowse(ActionEvent event) {
        System.out.println("handleBtnBrowseFile");
        FileChooser fc = new FileChooser();
        try{
            ifile = fc.showOpenDialog(null);
            txtInputFile.textProperty().setValue(ifile.getName() + "  ("+ifile.getPath()+")");
        }catch(Exception e){
            System.out.println("Error opening file ... " + e);
        }
    }
    
    
    @FXML
    private void handleBtnEdit(ActionEvent event){
        System.out.println("handleBtnEditSelected starting");
        
        try{
            final Integer s = tblResults.getSelectionModel().getSelectedIndex();        
            final LastFmSearch a = tblResults.getItems().get(s);
            System.out.println(tblResults.getItems().get(s).getTopArtist());
            
            
            final Stage newStage = new Stage();
            VBox v = new VBox();
            Label lblDescr = new Label("Below are the rest of the results for '"+a.getSearchTerm()+"'.");
            Label lblDescr2 = new Label("Select a better match from below.");
            
            //new search
            HBox searchHBox = new HBox();
            final TextField searchField = new TextField("Name");
            Button searchButton = new Button("New Search");
            searchHBox.getChildren().addAll(searchField,searchButton);
            
            final ListView<String> lv = new ListView();
            for (String selecteda : a.getAllArtists()){
                lv.getItems().add(selecteda);
            }            
            
            searchButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override 
                public void handle(ActionEvent e) {
                    if (searchField.getText().trim().isEmpty()){
                        System.out.println("Nothing entered in new search box");
                    }
                    else{
                        String ns = searchField.getText().trim();
                        LastFmSearch nl = new LastFmSearch();
                        try {
                            nl.queryArtist(ns);
                            lv.getItems().clear();
                            a.queryArtist(ns);
                            for (String selecteda : nl.getAllArtists()){
                                lv.getItems().add(selecteda);
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(ProMusicController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }                    
                }
            });
            
            
            
            Button btnReplace = new Button("OK");
            btnReplace.setOnAction(new EventHandler<ActionEvent>() {
                @Override 
                public void handle(ActionEvent e) {
                    boolean isok = false;
                    if (lv.getSelectionModel().isEmpty()){
                        
                    }
                    else{
                        Integer m = lv.getSelectionModel().getSelectedIndex();
                        System.out.println(lv.getItems().get(m));
                        a.setChosenArtist(lv.getItems().get(m));
                    }
                    tblResults.getItems().set(s,a);
                    newStage.close();
                    
                }
            });
              
            
            
            
            
            
            v.getChildren().addAll(lblDescr,lblDescr2,searchHBox,lv,btnReplace);
            Scene stageScene = new Scene(v, 400, 400);
            newStage.setScene(stageScene);
            newStage.show();
            
            
            
            
        }catch(Exception e){
            System.out.println("No selection?");
        }
        
        System.out.println("handleBtnEditSelected leaving");
    }
    
    
    
}


//Thread Factory that allows naming of the threads
class ProMusicThreadFactory implements ThreadFactory{
//        private final String name;
        public ProMusicThreadFactory(){
        }

    public Thread newThread(Runnable r) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
    }
}

