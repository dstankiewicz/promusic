/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.dstankiewicz;

import com.google.common.reflect.TypeToken;
import com.google.gdata.util.ServiceException;
import com.google.gson.Gson;
import de.umass.lastfm.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author asdf
 */
public class LastFmSearch {
    
    private final String apikey = "ada74bdf2e21ad7d281082bb03bf904b";
    Collection<Artist> artists;
    private String searchTerm;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String originalSearchTerm) {
        this.searchTerm = originalSearchTerm;
    }
    private String chosenArtist;
    
    public LastFmSearch(){
        chosenArtist = "-";
        searchTerm = "";
    }

    public String getTopArtist(){
        Iterator iter = artists.iterator();
        Artist a = (Artist) iter.next();
        return a.getName();
    }

    public String getChosenArtist(){
        return chosenArtist;
    }
    public void setChosenArtist(String ca){
        chosenArtist = ca;
    }
    
    public boolean queryArtist(String artist) throws IOException{
        System.out.println("LastFmSearch.queryArtist");
        try{
            searchTerm = artist;
            artists = (Collection<Artist>)(Collection<?>)Artist.search(artist, apikey);
            if (!artists.isEmpty()){
                chosenArtist = getTopArtist();
            }
//                for (Artist t : artists){
//                    System.out.println(t.getName());
//                }
        }
        catch (Exception e) {
            System.out.println("ERROR in search"+e);
        }
        System.out.println("Leaving LastFmSearch.queryArtist");
        return (0<artists.size());
    }
        
        public ArrayList<String> queryTracks(String artist) throws IOException{
            ArrayList<String> result = new ArrayList();
            try{

    //            URL u = new URL("http://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks&artist=led+zeppelin&api_key=ada74bdf2e21ad7d281082bb03bf904b&format=json");
    ////            URLConnection uc = u.openConnection();
    //            InputStream is = u.openStream();
    //            InputStreamReader isr = new InputStreamReader(is,"UTF-8");
    ////            Map<String, String> json = new Gson().fromJson(isr,new TypeToken<Map<String, String>>(){}.getType());


                Collection<Track> a = Artist.getTopTracks(artist, apikey);
                for (Track t : a){
                    System.out.println(t.getName());
                    result.add(t.getArtist() + " - " + t.getName());
                }


            } catch (Exception e) {
                System.out.println("ERROR in search"+e);
            }
            return result;
    }

    public ArrayList<String> getAllArtists() {
        ArrayList<String> as = new ArrayList();
        Iterator iter = artists.iterator();
        while (iter.hasNext()){
            Artist a = (Artist) iter.next();
            as.add(a.getName());
        }
        return as;
    }
    
}
