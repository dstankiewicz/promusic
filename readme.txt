Promusic
Dan Stankiewicz 
8/12/2014

This program allows the user to search lastfm for an artist(s) via a semicolon-delimited file or via the search box at the top.

Search results are in the first listview.  A result can be edited to a better match (the results of the search for each artist are preserved) or it can be replaced via a new search.  The edit is slipped into the results in place of the old one.

Results can be removedindividually or removed all at once.  Results can be downloaded all at once, or a few at a time.

Once a download dir is selected, youtube is queried for the tracks lastfm returned for that artist.  The results are placed in a text file lnamed Beatles.txt or the like.