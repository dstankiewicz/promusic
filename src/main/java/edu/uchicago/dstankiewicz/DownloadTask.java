/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.dstankiewicz;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressIndicator;

class DownloadTask extends Task<Void> {

        private String artist;
        private File odir;
        
        public DownloadTask(String artist, File odir) {
            this.artist = artist;
            this.odir = odir;
            this.updateMessage("Queued");
        }
        
        private boolean saveToFile(String str) throws IOException{
            boolean result = false;
            try{
                Files.write(Paths.get(odir+"/"+artist+".txt"), str.getBytes());
                result = true;
            }catch(IOException ioe){
                System.out.println("Problem saving file!" + ioe.getMessage());
            }
            return result;
        }

        @Override
        protected Void call() throws Exception {

            try {
                this.updateProgress(ProgressIndicator.INDETERMINATE_PROGRESS, 1);
                this.updateMessage("Queued");
                this.updateTitle(artist);
                
                LastFmSearch lfms = new LastFmSearch();
                ArrayList<String> tracks = lfms.queryTracks(artist);
                
                this.updateMessage("Searching YouTube ...");
                
                YouTubeSearch yts = new YouTubeSearch();
                double x = 0;
                int percentCompleted = 0;
                this.updateMessage("Searching YouTube ...");
                
                String results = "";
                
                for (String track : tracks) {
                    x = x+1.0;
                    ArrayList<YouTubeResult> t = yts.query(track, 1);
                    if (1 == t.size()){
                        results += track + " : " + t.get(0).getUrl() + "\n";
//                        System.out.println(track + " : " + t.get(0).getUrl() + " .... " + x + " " + percentCompleted);
                    }
                    else{
                        results += track + " : no-results-found-on-youtube\n";
                        System.out.println(track + " : no-results-found-on-youtube");
                    }
                    percentCompleted = (int) (100 * x / tracks.size());
                    updateProgress(percentCompleted, 100);
                }

                this.updateMessage(artist + "-" + "Saving file ...");
                boolean b = saveToFile(results);
                if (b){
                    this.updateMessage("Done");    
                }else{
                    this.updateMessage("Error saving file");    
                }
                


            } catch (IOException ex) {
                boolean cancel = this.cancel(true);
//                table.getItems().remove(this);

            }

            return null;
        }

    }
